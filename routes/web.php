<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('login',  function () {
    return view('login');
});

Route::get('main',  ['as' => 'main', function () {
    return view('main');
}]);

Route::get('profile',  ['as' => 'profile', function () {
    return view('profile');
}]);

Route::view('/profile', 'profile');

Route::view('/ticket_detail', 'ticketDetail');

Route::get('ticket_detail/{incidence?}', function ($incidence) {
    return view('ticketDetail', ['incidence' => $incidence]);
});

Route::get('users', function(){
	return App\User::all();
});
