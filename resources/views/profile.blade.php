@extends('layouts.app')
@section('content')
<div class="row">
	<div class="col m8 z-depth-2 center-align" style="margin-top: 48px;background-color: #fffdfb;">
		<div class="row" style="margin-top: 28px;">
			<div class="col m4" >
				<div class="row">
					<img src="{{ asset('images/profile-image.png')}}" class="circle center-align" style="width: 200px;height: 200px;margin: 0 auto;display: block;">
				</div>
				<div class="row">
					<a class="btn-floating btn-large waves-effect waves-light grey center-align tooltipped" data-position="bottom" data-delay="50" data-tooltip="Nº de incidencias creadas" style="margin: 0 auto;display: block;">
						<h5>3</h5>
					</a>
				</div>
			</div>
			<div class="col m8" style="border-left: 1px solid #e0e0e0">
				<div class="row">
					<h3 style="border-bottom: 1px solid #e0e0e0">Profesor Protón</h3>
				</div>
				<div class="row">
					<div class="col m6">
						<p class="center-align">Email:</p>
						<p>laracroft@gmail.com</p>
					</div>
					<div class="col m6">
						<p>Contraseña:</p>
						<p>************</p>
					</div>
				</div>
				<div class="row">
					<div class="col m6">
						<p>Edad:</p>
						<p>20</p>
					</div>
					<div class="col m6">
						<p>Rol:</p>
						<p>Professor/a</p>
					</div>
				</div>
			</div>
		</div>

	</div>
	<div class="col s3 z-depth-2" style="float: right; background-color: #fffdfb; margin-top: 48px;">
		<h5 style="text-align: center;">Notificaciones</h5>
		<div class="col s12 m12">
          <div class="card  teal lighten-5">
            <div class="card-content white-text">
              <span class="card-title" style="color: #00897b">Incidencia resuelta</span>
              <p  style="color: #00897b">El técnico al cargo ha resuleto tu incidencia.</p>
            </div>
            <div class="card-action">
              <a href="#" style="color: #00897b"><b>Ver incidencia</b></a>
            </div>
          </div>
        </div>
       <div class="col s12 m12">
          <div class="card  teal lighten-5">
            <div class="card-content white-text">
              <span class="card-title" style="color: #00897b">Incidencia resuelta</span>
              <p  style="color: #00897b">El técnico al cargo ha resuleto tu incidencia.</p>
            </div>
            <div class="card-action">
              <a href="#" style="color: #00897b"><b>Ver incidencia</b></a>
            </div>
          </div>
        </div>
        <div class="col s12 m12">
          <div class="card  light-blue lighten-5">
            <div class="card-content white-text">
              <span class="card-title" style="color: #039be5">Mensaje de Oriol</span>
              <p style="color: #039be5">No encuentro el ordenador al que te refieres, todos funcionan correctamente</p>
            </div>
            <div class="card-action">
              <a href="#" style="color: #039be5"><b>Ver mensaje</b></a>
            </div>
          </div>
        </div>
	</div>
	<div class="col m8 z-depth-2 center-align" style="margin-top: 48px;background-color: #fffdfb;">
	<h5 style="text-align: center;">Mis incidencias</h5>
		<table class="highlight">
			<thead>
				<tr>
					<th style="width: 65%">Titulo</th>
					<th>Técnico</th>
					<th style="text-align: right;">Estado</th>
				</tr>
			</thead>

			<tbody>
				<tr>
					<td>Error en la conexión con nuestro sistema firewall</td>
					<td><div class="chip" style="margin-left: auto;">
						<img src="http://www.pvhc.net/img240/uyttxprhsqycyximpzjb.png">
						<b>Guillem</b>
					</div>
				</div></td>
				<td><span class="new badge teal lighten-3" data-badge-caption="">Listo</span></td>
			</tr>
			<tr>
				<td>El ordenador del fondo no funciona y no tiene ratón</td>
				<td><div class="chip" style="margin-left: auto;">
					<img src="http://www.pvhc.net/img240/uyttxprhsqycyximpzjb.png">
					<b>Oriol</b>
				</div>
			</div></td>
			<td><span class="new badge light-blue lighten-3" data-badge-caption="">Progress</span></td>
		</tr>
		<tr>
			<td>Proyector con baja resolución, imagen distorsionada</td>
			<td><div class="chip" style="margin-left: auto;">
				<img src="http://www.pvhc.net/img240/uyttxprhsqycyximpzjb.png">
				<b>Raymon</b>
			</div>
		</div></td>
		<td><span class="new badge teal lighten-3" data-badge-caption="">Listo</span></td>
	</tr>
</tbody>
</table>
</div>
</div>
@stop
