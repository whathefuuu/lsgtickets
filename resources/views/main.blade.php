@extends('layouts.app')
@section('content')

<h3 style="border-bottom: 1px solid #e0e0e0">Bienvenido, <b>Profesor Protón</b></h3>
<div class="row">
	<div class="col s8 z-depth-2" style="margin-bottom: 35px; background-color: #fffdfb">
		<div class="input-field col s4 m4">
			<input placeholder="Búsqueda por tipo" id="first_name" type="text" class="validate">
		</div>
		<div class="input-field col s4 m4">
			<select multiple="">
				<option value="" disabled selected>Estado actual</option>
				<option value="">Resuelta</option>
				<option value="">En curso</option>
				<option value="">Sin asignar</option>
			</select>
		</div>
		<div class="input-field col s4 m4">
			<select class="icons">
				<option value="" disabled selected>Técnico al cargo</option>
				<option value="" data-icon="https://www.vbout.com/images/persona/buyer-persona-image1.png" class="circle">Jose Luís</option>
				<option value="" data-icon="https://www.vbout.com/images/persona/buyer-persona-image1.png" class="circle">Rodriguez</option>
				<option value="" data-icon="https://www.vbout.com/images/persona/buyer-persona-image1.png" class="circle">Zapatero</option>
			</select>
		</div>
	</div>
	<div class="col s3 z-depth-2" style="float: right; background-color: #fffdfb">
		<h5 style="text-align: center;">Situación general</h5>
		<ul class="collapsible" data-collapsible="accordion">
			<li class="active">
				<div class="collapsible-header active"><h6><b>Incidencias varias</b></h6><span class="new badge blue-grey lighten-3" data-badge-caption="">Wait</span></div>
				<div class="collapsible-body"><span>Breve descripción cutre</span></div>
			</li>
			<li>
				<div class="collapsible-header"><h6><b>No se pueden filtrar</b></h6><span class="new badge blue-grey lighten-3" data-badge-caption="">Wait</span></div>
				<div class="collapsible-body"><span>Lorem ipsum dolor sit amet.</span></div>
			</li>
			<li>
				<div class="collapsible-header"><h6><b>Por que no me apetece</b></h6><span class="new badge teal lighten-3" data-badge-caption="">Listo</span></div>
				<div class="collapsible-body"><span>Lorem ipsum dolor sit amet.</span></div>
			</li>
			<li>
				<div class="collapsible-header"><h6><b>Aunque podría bajarse</b></h6><span class="new badge light-blue lighten-3" data-badge-caption="">Progress</span></div>
				<div class="collapsible-body"><span>Lorem ipsum dolor sit amet.</span></div>
			</li>
			<li>
				<div class="collapsible-header"><h6><b>Y añadir un campo texto</b></h6><span class="new badge amber lighten-3" data-badge-caption="">Stop</span></div>
				<div class="collapsible-body"><span>Lorem ipsum dolor sit amet.</span></div>
			</li>
			<li>
				<div class="collapsible-header"><h6><b>Pero seria muy cutre</b></h6><span class="new badge teal lighten-3" data-badge-caption="">Listo</span></div>
				<div class="collapsible-body"><span>Lorem ipsum dolor sit amet.</span></div>
			</li>
		</ul>
	</div>
	<br>

	<div class="col s8 z-depth-2" style="background-color: #fffdfb">
		<table class="highlight">
			<thead>
				<tr>
					<th style="width: 65%">Titulo</th>
					<th>Técnico</th>
					<th style="text-align: right;">Estado</th>
				</tr>
			</thead>

			<tbody>
				<tr>
					<td>Error en la conexión con nuestro sistema firewall</td>
					<td>
						<div class="chip" style="margin-left: auto;">
							<img src="http://www.pvhc.net/img240/uyttxprhsqycyximpzjb.png">
							<b>Guillem</b>
						</div>
					</td>
					<td><span class="new badge teal lighten-3" data-badge-caption="">Listo</span></td>
				</tr>
				<tr>
				<td>El ordenador del fondo no funciona y no tiene ratón</td>
					<td>
						<div class="chip" style="margin-left: auto;">
							<img src="http://www.pvhc.net/img240/uyttxprhsqycyximpzjb.png">
							<b>Oriol</b>
						</div>
					</td>
					<td><span class="new badge light-blue lighten-3" data-badge-caption="">Progress</span></td>
				</tr>
				<tr>
					<td>Proyector con baja resolución, imagen distorsionada</td>
					<td><div class="chip" style="margin-left: auto;">
						<img src="http://www.pvhc.net/img240/uyttxprhsqycyximpzjb.png">
						<b>Raymon</b>
					</div>
				</td>
				<td><span class="new badge teal lighten-3" data-badge-caption="">Listo</span></td>
				</tr>
			</tbody>
		</table>
	</div>
</div>

<script>
	$(document).ready(function(){
		$('.collapsible').collapsible();
		$('select').material_select();
	});       
</script>

@stop