<!DOCTYPE html>
<html>
<head>
	<title>LSG Tickets</title>
	<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <!-- Compiled and minified CSS -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">

  <!-- Compiled and minified JavaScript -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
  <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
</head>
<style>
	@font-face {
  font-family: 'Roboto', sans-serif;
  font-style: normal;
  font-weight: 900;
}

#mobile-demo{
	background-color:  #0747a6;
}

#mobile-demo .side-nav li > a{
	color: white;
}
</style>
<body style="background-color: #eeeeee">
	<nav>
	    <div class="nav-wrapper" style="background-color: #0747a6">
	      <a href="#!" class="brand-logo" style="margin-left: 20px;">LSG Tickets</a>
	      <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
	      <ul class="right hide-on-med-and-down">
	        <li><a href="">Perfil</a></li>
	        <li><a href="badges.html">Notificaciones</a></li>
	      </ul>
	      <ul class="side-nav" id="mobile-demo">
	        <li><a href="sass.html">Perfil</a></li>
	        <li><a href="badges.html">Notificaciones</a></li>
	      </ul>
	    </div>
	  </nav>
	  <script>
	  	$( document ).ready(function(){
	  		$(".button-collapse").sideNav();
	  	})
	  </script>
	<div class="container">
	@yield('content')
	</div>
</body>
</html>