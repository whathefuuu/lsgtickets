@extends('layouts.app')
@section('content')

  <div class="section"></div>
  <main>
    <center>
      <h5 class="indigo-text"> Inicieu sessió al vostre compte </h5>
      <div class="section"></div>

      <div class="container">
        <div class="z-depth-1 grey lighten-4 row" style="display: inline-block; padding: 32px 48px 0px 48px; border: 1px solid #EEE;">

          <form class="col s12" method="post">
            <div class='row'>
              <div class='col s12'>
              </div>
            </div>

            <div class='row'>
              <div class='input-field col s12'>
                <input class='validate' type='email' name='email' id='email' />
                <label for='email'>Correu Electronic</label>
              </div>
            </div>

            <div class='row'>
              <div class='input-field col s12'>
                <input class='validate' type='password' name='password' id='password' />
                <label for='password'>Contrasenya</label>
              </div>
              <label style='float: right;'>
					<a class='pink-text' href='#!'><b>Ha oblidat la contrasenya?</b></a>
				</label>
            </div>

            <br />
            <center>
              <div class='row'>
                <a href="{{route('main')}}"><button type='button' name='btn_login' class='col s12 btn btn-large waves-effect light-blue darken-2'>Accedir</button>
                </a>
              </div>
            </center>
          </form>
        </div>
      </div>
      <a href="">Com obtenir un compte?</a>
    </center>
  </main>
@stop